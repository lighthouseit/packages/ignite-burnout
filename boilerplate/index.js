import 'moment/min/locales.min';
import 'react-native-gesture-handler';
import React from 'react';
import dva, { createRouter } from '@lighthouseapps/react-native-dva';
import moment from 'moment-timezone';
import models from './src/models';
import router from './src/router';
import App from './src/app.component';
import { name } from './app.json';

moment.tz.setDefault('America/Sao_Paulo');
moment.locale('pt-br');

const {
  routerMiddleware,
  routerReducer,
  Router,
} = createRouter(router);

const app = dva({
  models,
  extraReducers: {
    router: routerReducer,
  },
  onAction: [
    routerMiddleware,
  ],
});

app.start(name, (
  <App>
    <Router />
  </App>
));
