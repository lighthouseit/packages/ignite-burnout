## Visão Geral
### Quais são os itens do hotfix?
(Liste os itens (features, bugfixes, changes...) que entram a partir do hotfix)
* Item 1;
* Item 2;
* Item 3.

(Obs.: Se possível linke os itens com o trello/jira)

## Informações Adicionais
(Informações adicionais sobre o hotfix, como motivos da criação, etc)