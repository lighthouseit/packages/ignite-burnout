## Visão Geral
### O que é a feature?
(Descreva a feature)

### Como foi implementada?
(Descreva como foi implementada a feature)

### Que áreas da aplicação ela impacta?
(Descreva as áreas impactas e se houver, algum código que teve que ser alterado)

## Testes
### Como testar?
(Descreva os pré-requisitos e descreva os passos para testar a feature)

## Informações Adicionais
(Informações adicionais sobre a feature, caso hajam)