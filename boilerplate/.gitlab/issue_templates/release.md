## Visão Geral
### Quais são os itens da release?
(Liste os itens (features, bugfixes, changes...) que entram a partir da release)
* Item 1;
* Item 2;
* Item 3.

(Obs.: Se possível linke os itens com o trello/jira)

## Informações Adicionais
(Informações adicionais sobre a release)