## Visão Geral
### O que é o bug?
(Descreva o bug)

### Qual é o resultado esperado?
(Descreva o resultado esperado após resolução do bug)

### O que causava o bug?
(Descreva o que estava causando o bug)

### Como foi resolvido?
(Descreva como foi resolvido o bug)

## Testes
### Como testar?
(Descreva os pré-requisitos e descreva os passos para testar a feature)

## Informações Adicionais
(Informações adicionais sobre o bug, como prints e etc, caso hajam)