module.exports = {
  extends: ['airbnb', 'airbnb/hooks'],
  plugins: ['react', 'react-native'],
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module',
  },
  rules: {
    "linebreak-style": 0,
    'react/prop-types': 'off',
    'react/jsx-props-no-spreading': 'off',
    'no-console': 'off',
    'import/prefer-default-export': 'off',
    'comma-dangle': ['error', 'always-multiline'],
    'react/jsx-filename-extension': [
      'error',
      { 'extensions': ['.js', '.jsx'] },
    ],
  },
  settings: {
    'import/resolver': {
      'babel-plugin-root-import': {
        'rootPathPrefix': '@/',
        'rootPathSuffix': './src/',
      },
    },
  },
};
