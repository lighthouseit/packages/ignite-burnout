# <%= props.name %>

[![airbnb-javascript-style](https://img.shields.io/badge/code%20style-airbnb-brightgreen.svg?style=flat)](https://github.com/airbnb/javascript)

Aplicativo gerado usando [Ignite Lighthouse Burnout](https://gitlab.com/lighthouseit/labs/ignite-burnout)

## :arrow_forward: Rodando app

```
// Running on Android
$ yarn androi

// Running on iOS
$ cd ios && pod install
$ yarn ios

🚀  1, 2, 3... Ready!
```

## Configurar Firebase (Obrigatório)

Seguir as instruções das documentaçãos abaixo:

1- [Criando projeto no Firebase](https://invertase.io/oss/react-native-firebase/quick-start/create-firebase-project)
2- [Android](https://invertase.io/oss/react-native-firebase/quick-start/android-firebase-credentials)
3- [iOS](https://invertase.io/oss/react-native-firebase/quick-start/ios-firebase-credentials)
