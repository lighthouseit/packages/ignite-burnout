import React from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useTheme } from '@ui-kitten/components';
import styled from 'styled-components';

export const Container = styled(SafeAreaView)`
  flex: 1;
  background-color: ${({ theme }) => theme['background-basic-color-2']};
`;

export default ({ children }) => {
  const theme = useTheme();

  return (
    <Container theme={theme}>
      {children}
    </Container>
  );
};
