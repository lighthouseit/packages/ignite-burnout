export default {
  namespace: 'app',
  state: {
    theme: 'light',
  },
  effects: {},
  reducers: {
    changeTheme(state, { payload }) {
      return {
        ...state,
        theme: payload,
      };
    },
  },
  subscriptions: {},
};
