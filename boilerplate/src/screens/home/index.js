import React from 'react';
import { useTheme, TopNavigation } from '@ui-kitten/components';
import AppLayout from '@/layouts/App';

import { Container } from './styles';

export default () => {
  const theme = useTheme();

  return (
    <AppLayout>
      <TopNavigation title="Home" alignment="center" />
      <Container theme={theme} />
    </AppLayout>
  );
};
