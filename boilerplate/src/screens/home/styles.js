/* eslint-disable import/prefer-default-export */
import styled from 'styled-components';

export const Container = styled.View`
  flex: 1;
  background-color: ${({ theme }) => theme['color-basic-300']};
`;
