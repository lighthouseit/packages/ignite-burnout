import React, { useEffect } from 'react';
import { useTheme, Spinner } from '@ui-kitten/components';
import { connect } from '@lighthouseapps/react-native-dva';
import AppLayout from '@/layouts/App';

import { Container } from './styles';

const Index = ({ navigation }) => {
  const theme = useTheme();

  useEffect(() => {
    const listener = setTimeout(() => navigation.navigate('App'), 1000);
    return () => clearTimeout(listener);
  }, [navigation]);

  return (
    <AppLayout>
      <Container theme={theme}>
        <Spinner />
      </Container>
    </AppLayout>
  );
};

export default connect()(Index);
