import React from 'react';
import { SafeAreaView } from 'react-native';
import { BottomNavigation, BottomNavigationTab, Icon } from '@ui-kitten/components';

export default ({ navigation, routes }) => {
  const onSelect = (index) => {
    navigation.navigate(routes[index].name);
  };

  return (
    <SafeAreaView>
      <BottomNavigation
        selectedIndex={navigation.state.index}
        onSelect={onSelect}
        appearance="noIndicator"
      >
        {routes.map((route) => (
          <BottomNavigationTab
            key={route.name}
            title={route.label}
            icon={(style) => <Icon {...style} name={route.icon} />}
          />
        ))}
      </BottomNavigation>
    </SafeAreaView>
  );
};
