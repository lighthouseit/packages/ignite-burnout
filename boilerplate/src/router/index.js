import React from 'react';

// Layouts
import AppLayout from '@/layouts/App';

// Screens
import Loading from '@/screens/loading';
import Home from '@/screens/home';
import Account from '@/screens/account';

import BottomTabBar from '@/components/BottomTabBar';
import { statusBarColor } from './configs';

export default {
  type: 'switch',
  routes: [
    {
      type: 'screen',
      name: 'Loading',
      component: Loading,
      params: {
        statusBar: statusBarColor,
      },
    },
    {
      type: 'stack',
      name: 'App',
      routes: [
        {
          type: 'tab-bottom',
          name: 'Root',
          routes: [
            {
              type: 'screen',
              name: 'Home',
              component: Home,
              label: 'Início',
              icon: 'home-outline',
              params: {
                statusBar: statusBarColor,
              },
            },
            {
              type: 'screen',
              name: 'Account',
              component: Account,
              label: 'Conta',
              icon: 'person-outline',
              params: {
                statusBar: statusBarColor,
              },
            },
          ],
          options: {
            tabBarComponent: (props) => <BottomTabBar {...props} />,
          },
        },
      ],
    },
  ],
  layout: AppLayout,
};
