import React from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { connect } from '@lighthouseapps/react-native-dva';
import { light as lightTheme, dark as darkTheme, mapping } from '@eva-design/eva';
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components';
import { EvaIconsPack } from '@ui-kitten/eva-icons';

const App = ({ children, theme }) => (
  <>
    <IconRegistry icons={EvaIconsPack} />
    <ApplicationProvider
      mapping={mapping}
      theme={theme === 'light' ? lightTheme : darkTheme}
    >
      <SafeAreaProvider>
        {children}
      </SafeAreaProvider>
    </ApplicationProvider>
  </>
);

export default connect(({ app }) => ({ theme: app.theme }))(App);
