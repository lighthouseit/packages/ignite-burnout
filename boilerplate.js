const { mergeDeepRight, pipe, assoc, omit, __ } = require('ramda')
const { getReactNativeVersion } = require('./lib/react-native-version')

// We need this value here, as well as in our package.json.ejs template
const REACT_NATIVE_GESTURE_HANDLER_VERSION = "^1.5.2"

/**
 * Is Android installed?
 *
 * $ANDROID_HOME/tools folder has to exist.
 *
 * @param {*} context - The gluegun context.
 * @returns {boolean}
 */
const isAndroidInstalled = function (context) {
  const androidHome = process.env['ANDROID_HOME']
  const hasAndroidEnv = !context.strings.isBlank(androidHome)
  const hasAndroid = hasAndroidEnv && context.filesystem.exists(`${androidHome}/tools`) === 'dir'

  return Boolean(hasAndroid)
}

/**
 * Let's install.
 *
 * @param {any} context - The gluegun context.
 */
async function install(context) {
  const {
    filesystem,
    patching,
    parameters,
    ignite,
    reactNative,
    print,
    system,
    prompt,
    template
  } = context
  const { colors } = print
  const { red, yellow, bold, gray, blue } = colors
  const isWindows = process.platform === "win32"
  const isMac = process.platform === "darwin"

  const perfStart = new Date().getTime()

  const name = parameters.first
  const spinner = print
    .spin(`using the ${yellow('Lighthouse')} boilerplate (code name '${red('Burnout')}')`)
    .succeed()

  // attempt to install React Native or die trying
  const useNpm = !ignite.useYarn
  const rnInstall = await reactNative.install({
    name,
    version: getReactNativeVersion(context),
    useNpm
  })
  if (rnInstall.exitCode > 0) process.exit(rnInstall.exitCode)

  // remove the __tests__ directory and App.js that come with React Native
  filesystem.remove('__tests__')
  filesystem.remove('App.js')

  // copy our App, Tests & storybook directories
  spinner.text = '▸ copying files'
  spinner.start()
  filesystem.copy(`${__dirname}/boilerplate/src`, `${process.cwd()}/src`, {
    overwrite: true,
    matching: '!*.ejs'
  })
  filesystem.dir(`${process.cwd()}/ignite`)
  spinner.stop()

  // generate some templates
  spinner.text = '▸ generating files'

  const templates = [
    { template: 'index.js', target: 'index.js' },
    { template: 'README.md', target: 'README.md' },
    { template: 'ignite.json.ejs', target: 'ignite/ignite.json' },
    { template: '.editorconfig', target: '.editorconfig' },
    { template: 'babel.config.js', target: 'babel.config.js' },
    { template: '.eslintrc.js', target: '.eslintrc.js' },
    { template: '.prettierrc.js', target: '.prettierrc.js' },
    { template: 'Gemfile', target: 'Gemfile' },
    { template: 'jsconfig.json', target: 'jsconfig.json' },
  ]

  const templateProps = {
    name,
    igniteVersion: ignite.version,
    reactNativeVersion: rnInstall.version,
    reactNativeGestureHandlerVersion: REACT_NATIVE_GESTURE_HANDLER_VERSION
  }
  await ignite.copyBatch(context, templates, templateProps, {
    quiet: !parameters.options.debug,
    directory: `${ignite.ignitePluginPath()}/boilerplate`
  })

  /**
   * Append to files
   */
  // https://github.com/facebook/react-native/issues/12724
  await filesystem.appendAsync('.gitattributes', '*.bat text eol=crlf')
  filesystem.append('.gitignore', '\n# Misc\n#')
  filesystem.append('.gitignore', '\n.env\n')

  // transform our package.json in case we need to replace variables
  const rawJson = await template.generate({
    directory: `${ignite.ignitePluginPath()}/boilerplate`,
    template: 'package.json.ejs',
    props: templateProps
  })
  const newPackageJson = JSON.parse(rawJson)

  // read in the react-native created package.json
  const currentPackage = filesystem.read('package.json', 'json')

  // deep merge, lol
  const newPackage = pipe(
    assoc('dependencies', mergeDeepRight(currentPackage.dependencies, newPackageJson.dependencies)),
    assoc('devDependencies', mergeDeepRight(currentPackage.devDependencies, newPackageJson.devDependencies)),
    assoc('scripts', mergeDeepRight(currentPackage.scripts, newPackageJson.scripts)),
    mergeDeepRight(__, omit(['dependencies', 'devDependencies', 'scripts'], newPackageJson))
  )(currentPackage)

  // write this out
  filesystem.write('package.json', newPackage, { jsonIndent: 2 })

  spinner.stop()

  // pass long the debug flag if we're running in that mode
  const debugFlag = parameters.options.debug ? '--debug' : ''

  /**
   * Install react-native-gesture-handler and add required native code needed
   */
  try {
    await ignite.addModule("react-native-gesture-handler", {
      version: REACT_NATIVE_GESTURE_HANDLER_VERSION,
      link: false
    })

    ignite.patchInFile(
      `${process.cwd()}/android/app/src/main/java/com/${name.toLowerCase()}/MainActivity.java`,
      {
        after: "import com.facebook.react.ReactActivity;",
        insert: `
      import com.facebook.react.ReactActivityDelegate;
      import com.facebook.react.ReactRootView;
      import com.swmansion.gesturehandler.react.RNGestureHandlerEnabledRootView;`,
      },
    )

    ignite.patchInFile(
      `${process.cwd()}/android/app/src/main/java/com/${name.toLowerCase()}/MainActivity.java`,
      {
        after: `public class MainActivity extends ReactActivity {`,
        insert:
          "\n  @Override\n" +
          "  protected ReactActivityDelegate createReactActivityDelegate() {\n" +
          "    return new ReactActivityDelegate(this, getMainComponentName()) {\n" +
          "      @Override\n" +
          "      protected ReactRootView createRootView() {\n" +
          "       return new RNGestureHandlerEnabledRootView(MainActivity.this);\n" +
          "      }\n" +
          "    };\n" +
          "  }",
      },
    )
  } catch (e) {
    ignite.log(e)
    throw e
  }

  /**
   * Add native code needed for React Native Vector Icons to build.gradle
   * Android only
   */
  try {
    await ignite.patchInFile(`${process.cwd()}/android/app/build.gradle`, {
      after: `enableHermes: false,  // clean and rebuild if changing\n]`,
      insert: `\napply from: "../../node_modules/react-native-vector-icons/fonts.gradle"`
    })
  } catch (e) {
    ignite.log(e)
    throw e
  }

  // git configuration
  const gitExists = await filesystem.exists('./.git')
  if (!gitExists && !parameters.options['skip-git'] && system.which('git')) {
    // initial git
    const spinner = print.spin('configuring git')

    // TODO: Make husky hooks optional
    const huskyCmd = '' // `&& node node_modules/husky/bin/install .`
    await system.run(`git init . && git add . && git commit -m "Initial commit." ${huskyCmd}`)

    spinner.succeed(`configured git`)
  }

  const perfDuration = parseInt((new Date().getTime() - perfStart) / 10) / 100
  spinner.succeed(`ignited ${yellow(name)} in ${perfDuration}s`)

  const androidInfo = isAndroidInstalled(context)
    ? ''
    : `\n\nTo run in Android, make sure you've followed the latest react-native setup instructions at https://facebook.github.io/react-native/docs/getting-started.html before using ignite.\nYou won't be able to run ${bold(
      'react-native run-android'
    )} successfully until you have.`

  const successMessage = `
    ${red('Ignite CLI')} ignited ${yellow(name)} in ${gray(`${perfDuration}s`)}

    // Running on Android
    yarn android

    // Running on iOS
    yarn ios

    To know more about the Ignite CLI, run ${red('ignite --help')}

    ${yellow(bold('🚀  1, 2, 3... Ready!'))}
  `

  print.info(successMessage)
}

module.exports = {
  install
}
