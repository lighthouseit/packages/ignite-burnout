# Ignite Lighthouse Burnout

![Lighthouse Burnout](https://i.imgur.com/VpKKfIM.png)

Ignite Lighthouse Burnout é um template para o Ignite CLI que integra as melhoras práticas usadas pelos nossos desenvolvedores.

## Main Stack

- React Native 0.61.5
- React Navigation 4.x
- [React Native UI Kitten](https://github.com/akveo/react-native-ui-kitten)
- [React Native DVA](https://www.npmjs.com/package/@lighthouseapps/react-native-dva)
- [Styled Components](https://styled-components.com/)

[Node.js 8.16.0+](https://nodejs.org) é obrigatório.

## Início

Primeiramente, instale o `Ignite CLI`:

```
yarn global add ignite-cli
```

Iniciando um novo projeto

```
ignite new MyApp -b lighthouse-burnout

// Running on Android
$ yarn android

// Running on iOS
$ cd ios && pod install
$ yarn ios

🚀  1, 2, 3... Ready!
```

## Extra

[Burnout](https://gitlab.com/lighthouseit/labs/ignite-burnout) é um boilerplate para React Native desenvolvido pela [Lighthouse](https://lighthouseit.com.br/), um time de designers e desenvolvedores localizado em Porto Alegre / RS.
